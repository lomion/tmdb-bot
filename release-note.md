# Release 1.3.0

Features:
- Added vote average to results
- Added details to popular
- Reply to popular list for movie details

Supported commands:
- Use !tmdb help to get help about the bot usage.
- Use !tmdb movie {title} [y:{release year}] to get movie detail based on the given title.
- Use !tmdb tvshow {title} to get detail about a tv show based on the given title.
- Use !tmdb popular [{rating}] to get most popular movies. Get details about any one movie in the list by adding the {rating}.
- Use !tmdb language {language} to set your prefered language.
- Use !tmdb poster_size [{size}] to set your prefered poster size. With empty {size} all available sizes are listed.

