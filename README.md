[![Woodpecker-CI](https://drone.sarkasti.eu/api/badges/lomion/tmdb-bot/status.svg)](https://drone.sarkasti.eu/lomion/tmdb-bot)
# tmdb-bot
A [maubot](https://github.com/maubot/maubot) for Matrix messaging to get information about movies from [TheMovieDB.org](https://www.themoviedb.org/).

## Usage
Use `!tmdb help` to get help about the bot usage.

Use `!tmdb movie {title} [y:{release year}]` to get movie detail based on the given title.

Use `!tmdb tvshow {title}` to get detail about a tv show based on the given title.

Use `!tmdb popular [{rating}]` to get most popular movies. Get details about any one movie in the list by adding the {rating}.

Use `!tmdb language {language}` to set your prefered language.

Use `!tmdb poster_size [{size}]` to set your prefered poster size. With empty {size} all available sizes are listed.

## Screenshot
![Screenshot](screenshot_tmdb-bot-1.2.0.jpg)

## Discussion
Matrix room: [#tmdb-bot:matrix.sarkasti.eu](https://matrix.to/#/#tmdb-bot:matrix.sarkasti.eu)

## Avatar
Avatar icons was made by [inipagistudio](https://www.flaticon.com/authors/inipagistudio) from [www.flaticon.com](https://www.flaticon.com/)
