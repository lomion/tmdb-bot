from tmdb.tmdb_api import Movie
import asyncio
import time


async def test():
    start = time.time()
    movie = Movie()
    await movie.load_parameters()
    id = await movie.search_title('Breakfast Club')
    print("ID of Breakfast Club " + str(id))
    print(time.time() - start)
    await movie.close_session()

asyncio.run(test())
