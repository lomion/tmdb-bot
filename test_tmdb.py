#!/usr/bin/env python3
import unittest
from html import escape
from tmdb.tmdb_api import Movie, TvShow, MoviePopular
from tmdb.tmdb import TmdbBot
from tmdb.database import Database
from sqlalchemy import create_engine
import aiohttp


async def apiRequests(command):
    api_key = '51d75c00dc1502dc894b7773ec3e7a15'
    base_url = "https://api.themoviedb.org/3/"
    url = base_url + command.lstrip('/')
    params = {'api_key': api_key}
    params.update({'language': 'en'})
    async with aiohttp.ClientSession() as client:
        async with client.get(url, params=params) as resp:
            return await resp.json()


class TestTmdbMethods(unittest.IsolatedAsyncioTestCase):
    # TMDB API
    async def test_search_item(self):
        movie = Movie()
        await movie.load_parameters()
        id = await movie.search_title('Breakfast Club')
        self.assertEqual(id, 2108)
        self.assertEqual(movie.valid, True)
        await movie.close_session()

    async def test_vote(self):
        movie = Movie()
        await movie.load_parameters()
        await movie.search_title('Dune')
        vote = movie.vote_average
        self.assertEqual(vote, 7.8)
        await movie.close_session()

    async def test_cast(self):
        movie = Movie()
        await movie.load_parameters()
        await movie.search_title('Breakfast Club')
        self.assertEqual('Emilio Estevez', movie.cast[0])
        await movie.close_session()

    async def test_title(self):
        movie = Movie()
        await movie.load_parameters()
        await movie.search_title('Breakfast Club')
        self.assertEqual('The Breakfast Club', movie.title)
        await movie.close_session()

    async def test_overview(self):
        movie = Movie()
        await movie.load_parameters()
        await movie.search_title('Breakfast Club')
        description = 'Five high school students from different walks of'
        self.assertEqual(description, movie.overview[:len(description)])
        await movie.close_session()

    async def test_change_language(self):
        movie = Movie()
        await movie.load_parameters()
        movie.set_language('en')
        await movie.search_title('Breakfast Club')
        description = 'Five high school students from different walks of life endure a Saturday detention'
        self.assertEqual(description, movie.overview[:len(description)])
        await movie.close_session()

    def test_database_language(self):
        engine = create_engine('sqlite:///test.db', echo=True)
        db = Database(engine)
        db.set_language('@testuser:example.com', 'de')
        self.assertEqual(str(db.get_language('@testuser:example.com')), 'de')
        db.set_language('@testuser:example.com', 'en')
        self.assertEqual(str(db.get_language('@testuser:example.com')), 'en')

    async def test_id_lookup(self):
        movie = Movie()
        await movie.load_parameters()
        await movie.query_details('2108')
        self.assertEqual('The Breakfast Club', movie.title)
        await movie.close_session()

    async def test_search_fails(self):
        movie = Movie()
        await movie.load_parameters()
        id = await movie.search_title('Breakfast Club 2019')
        self.assertEqual(id, None)
        self.assertEqual(None, movie.title)
        self.assertEqual(movie.valid, False)
        await movie.close_session()

    async def test_search_year(self):
        movie = Movie()
        await movie.load_parameters()
        id = await movie.search_title('Dune')
        self.assertEqual(id, 438631)
        id = await movie.search_title('Dune', 1984)
        self.assertEqual(id, 841)
        await movie.close_session()

    def test_split_year(self):
        tmdb = TmdbBot("", "", "", "", "", "", "", "", "", "")
        title, year = tmdb.split_title_year('Dune')
        self.assertEqual('Dune', title)
        self.assertEqual(None, year)
        title, year = tmdb.split_title_year('Dune y:2020 ')
        self.assertEqual('Dune', title)
        self.assertEqual(2020, year)

    async def test_set_poster_size(self):
        movie = Movie()
        await movie.load_parameters()
        size = movie.set_poster_size("w500")
        self.assertEqual(size, "w500")
        self.assertEqual(movie.base_url_poster, f"{movie.base_url_images}w500")
        size = movie.set_poster_size("w666")
        self.assertEqual(size, None)
        await movie.close_session()

    async def test_year_no_y(self):
        movie = Movie()
        await movie.load_parameters()
        id = await movie.search_title('infinite 2021')
        self.assertEqual(id, None)
        self.assertEqual(movie.valid, False)

    # TV Shows
    async def test_search_tvshow(self):
        movie = TvShow()
        await movie.load_parameters()
        id = await movie.search_title('The Flash')
        self.assertEqual(id, 60735)
        await movie.close_session()

    async def test_tv_title(self):
        movie = TvShow()
        await movie.load_parameters()
        await movie.search_title('The Flash')
        self.assertEqual('The Flash', movie.title)
        await movie.close_session()

    async def test_cast_2(self):
        movie = TvShow()
        await movie.load_parameters()
        await movie.search_title('The Flash')
        self.assertEqual('Danielle Panabaker', movie.cast[2])
        await movie.close_session()

    async def test_poster_path(self):
        movie = Movie()
        await movie.load_parameters()
        await movie.search_title('Dune')
        self.assertEqual(movie.poster_url, "http://image.tmdb.org/t/p/w92/d5NXSklXo0qyIYkgV94XAgMIckC.jpg")
        await movie.close_session()

    async def test_movie_popular_length(self):
        results = await apiRequests('/movie/popular')
        list = MoviePopular()
        await list.load_parameters()
        text = await list.query()
        self.assertEqual(text, results['total_results'])
        await list.close_session()

    async def test_movie_popular_id(self):
        results = await apiRequests('/movie/popular')
        list = MoviePopular()
        await list.load_parameters()
        await list.query()
        self.assertEqual(list.list[2]['id'], results['results'][2]['id'])
        await list.close_session()

    async def test_movie_popular_text(self):
        results = await apiRequests('/movie/popular')
        list = MoviePopular()
        await list.load_parameters()
        await list.query()
        test_result = results['results'][-1]['title']
        tested = list.getListText()
        tested = tested[(len(results['results'][-1]['title'])) * -1:]
        self.assertEqual(tested, test_result)
        await list.close_session()



if __name__ == '__main__':
    unittest.main()
